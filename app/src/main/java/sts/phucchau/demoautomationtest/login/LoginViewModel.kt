package sts.phucchau.demoautomationtest.login

import com.base.viewmodel.FragmentViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import sts.phucchau.data.domain.AppDomain
import sts.phucchau.data.model.Customer

class LoginViewModel : FragmentViewModel() {

    private var mAppDomain: AppDomain? = null
    private var mCustomer: Customer? = null

    fun setViewModelAttributes(appDomain: AppDomain, customer: Customer) {
        mAppDomain = appDomain
        mCustomer = customer
    }

    fun onLogin() {
        val view: LoginView? = view()

        addDisposable(mAppDomain?.login(mCustomer!!.userName, mCustomer!!.password)!!
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.showLoading() }
                .doFinally { view?.hideLoading() }
                .subscribe({ view?.onLoginSuccessfully(it) }, { view?.onLoginFailed() }))
    }

    fun onRegister() {
        val view: LoginView? = view()
        view?.onShowRegister()
    }
}