package sts.phucchau.demoautomationtest.login

import com.base.viewmodel.CommonView

interface LoginView: CommonView {

    fun onLoginSuccessfully(code: Int)

    fun onLoginFailed()

    fun onShowRegister()
}