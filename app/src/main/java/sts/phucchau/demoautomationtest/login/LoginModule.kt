package sts.phucchau.demoautomationtest.login

import dagger.Module
import dagger.Provides
import sts.phucchau.data.model.Customer

@Module
class LoginModule {

    @Provides
    fun getCustomer(): Customer {
        return Customer()
    }
}