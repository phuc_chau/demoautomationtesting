package sts.phucchau.demoautomationtest.login

import android.util.Log
import android.widget.Toast
import sts.phucchau.data.domain.AppDomain
import com.base.fragment.BaseInjectingFragment
import sts.phucchau.data.model.Customer
import sts.phucchau.demoautomationtest.App
import sts.phucchau.demoautomationtest.R
import sts.phucchau.demoautomationtest.databinding.FragmentLoginBinding
import sts.phucchau.demoautomationtest.register.RegisterFragment
import javax.inject.Inject

class LoginFragment : BaseInjectingFragment<FragmentLoginBinding, LoginViewModel, LoginComponent>(), LoginView {

    @Inject
    lateinit var mCustomer: Customer

    override fun createComponent(): LoginComponent? {
        return DaggerLoginComponent.builder().appComponent(App.get(activity!!).component()).loginModule(LoginModule()).build()
    }

    override fun onInject(component: LoginComponent) {
        component.inject(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun getViewModelClass(): Class<LoginViewModel>? {
        return LoginViewModel::class.java
    }

    override fun onVisible() {
        super.onVisible()
        mViewDataBinding.viewModel = mViewModel
    }

    @Inject
    fun setViewModelAttributes(appDomain: AppDomain) {
        mViewModel?.setViewModelAttributes(appDomain, mCustomer)
    }

    override fun onLoginSuccessfully(code: Int) {
        when (code) {
            200 -> {
                Toast.makeText(activity, "Đăng nhập thành công!", Toast.LENGTH_SHORT).show()
                //Load user vao2 fragment home
            }
            201 -> {
                Toast.makeText(activity, "Sai tên đăng nhập hoặc mật khẩu", Toast.LENGTH_SHORT).show()
            }
            202 -> {
                Toast.makeText(activity, "Đăng nhập thất bại!", Toast.LENGTH_SHORT).show()
            }
            else -> {
                Log.e("result", code.toString())
            }
        }
    }

    override fun onLoginFailed() {
        Toast.makeText(activity, "Đăng nhập thất bại!", Toast.LENGTH_SHORT).show()
    }

    override fun onShowRegister() {
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.add(R.id.main_frame, RegisterFragment()).addToBackStack("home")
                .commit()
    }
}