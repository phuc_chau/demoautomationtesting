package sts.phucchau.demoautomationtest.login

import com.base.injection.scope.OutOfApplicationScope
import dagger.Component
import sts.phucchau.data.common.AppComponent

@Component(modules = [(LoginModule::class)], dependencies = [(AppComponent::class)])
@OutOfApplicationScope
interface LoginComponent {
    fun inject(i: LoginFragment)
}