package sts.phucchau.demoautomationtest.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.view.View

class Views {

    companion object {
        private val BITMAP_SCALE = 0.4f
        private val BLUR_RADIUS = 7.5f

        fun blur(v: View, scale: Float = BITMAP_SCALE, radius: Float = BLUR_RADIUS): Bitmap {
            return blur(v.context, getScreenshot(v), scale, radius)
        }

        fun blur(ctx: Context, image: Bitmap, scale: Float, radius: Float): Bitmap {
            val width = Math.round(image.width * scale)
            val height = Math.round(image.height * scale)

            val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
            val outputBitmap = Bitmap.createBitmap(inputBitmap)

            val rs = RenderScript.create(ctx)
            val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
            val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
            val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
            theIntrinsic.setRadius(radius)
            theIntrinsic.setInput(tmpIn)
            theIntrinsic.forEach(tmpOut)
            tmpOut.copyTo(outputBitmap)

            return outputBitmap
        }

        private fun getScreenshot(v: View): Bitmap {
            val b = Bitmap.createBitmap(v.width, v.height, Bitmap.Config.ARGB_8888)
            val c = Canvas(b)
            v.draw(c)
            return b
        }
    }
}