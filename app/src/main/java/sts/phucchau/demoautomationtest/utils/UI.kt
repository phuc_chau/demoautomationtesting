package sts.phucchau.demoautomationtest.utils

class UI
private constructor() {

    private val dp = 0

    companion object {

        private val instance = UI()

        fun dp(i: Int): Int {
            return instance.dp * i
        }
    }
}