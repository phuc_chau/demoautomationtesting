package sts.phucchau.demoautomationtest

import android.app.Activity
import android.app.Application
import android.content.Context
import android.graphics.Point
import android.util.TypedValue
import android.view.WindowManager
import sts.phucchau.data.common.AppComponent
import com.base.injection.module.AppContextModule
import sts.phucchau.data.common.DaggerAppComponent

class App : Application() {
    private lateinit var component: AppComponent

    private fun setDisplaySize() {
        val windowManager = applicationContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        setWidth(size.x)
        setHeight(size.y)
    }

    private fun setBaseDp() {
        dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics).toInt()
    }

    private fun setWidth(w: Int) {
        if (w > maxWidth) {
            maxWidth = w
        }
    }

    private fun setHeight(h: Int) {
        if (h > maxHeight) {
            maxHeight = h
        }
    }

    fun dp(factor: Int): Int {
        return dp * factor
    }

    companion object {
        var maxWidth = 0
        var maxHeight = 0
        var dp = 0

        fun get(activity: Activity): App {
            return App::class.java.cast(activity.application)
        }
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
                .appContextModule(AppContextModule(this))
                .build()

        setDisplaySize()

        /*CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SedgwickAve-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())*/
    }

    fun component(): AppComponent {
        return component
    }
}