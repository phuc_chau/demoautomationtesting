package sts.phucchau.demoautomationtest.first_register

import sts.phucchau.data.common.AppComponent
import com.base.injection.scope.OutOfApplicationScope
import dagger.Component

@Component(modules = [(FirstRegisterModule::class)], dependencies = [(AppComponent::class)])
@OutOfApplicationScope
interface FirstRegisterComponent {
    fun inject(i: FirstRegisterFragment)
}