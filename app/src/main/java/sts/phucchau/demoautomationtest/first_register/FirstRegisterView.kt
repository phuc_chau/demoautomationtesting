package sts.phucchau.demoautomationtest.first_register

import com.base.viewmodel.CommonView
import sts.phucchau.data.model.Customer

interface FirstRegisterView: CommonView{

    fun onNext(customer: Customer)

    fun onEmptyCustomerName()

    fun onEmptyEmail()

    fun onEmptyPhoneNumber()

    fun onWrongPhoneNumber()

    fun onWrongEmail()
}