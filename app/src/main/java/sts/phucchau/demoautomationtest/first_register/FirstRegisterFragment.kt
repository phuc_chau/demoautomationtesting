package sts.phucchau.demoautomationtest.first_register

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.base.fragment.BaseInjectingFragment
import sts.phucchau.data.model.Customer
import sts.phucchau.demoautomationtest.App
import sts.phucchau.demoautomationtest.R
import sts.phucchau.demoautomationtest.databinding.FragmentFirstRegisterBinding
import sts.phucchau.demoautomationtest.register.INextFragment

@SuppressLint("ValidFragment")
class FirstRegisterFragment(private val nextFragment: INextFragment): BaseInjectingFragment<FragmentFirstRegisterBinding, FirstRegisterViewModel, FirstRegisterComponent>(), FirstRegisterView {


    override fun createComponent(): FirstRegisterComponent? {
        return DaggerFirstRegisterComponent.builder().appComponent(App.get(activity!!).component()).build()
    }

    override fun onInject(component: FirstRegisterComponent) {
        component.inject(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_first_register
    }

    override fun getViewModelClass(): Class<FirstRegisterViewModel>? {
        return FirstRegisterViewModel::class.java
    }

    override fun onVisible() {
        super.onVisible()
        mViewDataBinding.viewModel = mViewModel

        mViewDataBinding.userNameET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.isNotEmpty()) {
                    if (mViewDataBinding.txtErrorUsername.visibility == View.VISIBLE) mViewDataBinding.txtErrorUsername.visibility = View.INVISIBLE
                }
            }
        })

        mViewDataBinding.mobilePhoneET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.length in 1..11) {
                    if (mViewDataBinding.txtErrorPhone.visibility == View.VISIBLE) mViewDataBinding.txtErrorPhone.visibility = View.INVISIBLE
                }

                if (editable.length == 12)
                    mViewDataBinding.txtErrorPhone.visibility = View.INVISIBLE
            }
        })

        mViewDataBinding.emailET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.isNotEmpty()) {
                    if (mViewDataBinding.txtErrorEmail.visibility == View.VISIBLE) mViewDataBinding.txtErrorEmail.visibility = View.INVISIBLE
                }
            }
        })
    }

    override fun onNext(customer: Customer) {
        nextFragment.nextFragment(mViewDataBinding.backBtn, customer)
    }

    override fun onEmptyEmail() {
        mViewDataBinding.txtErrorEmail.text = "Xin hãy nhập thông tin"
        mViewDataBinding.txtErrorEmail.visibility = View.VISIBLE
    }

    override fun onWrongEmail() {
        mViewDataBinding.txtErrorEmail.text = "Xin hãy nhập đúng email"
        mViewDataBinding.txtErrorEmail.visibility = View.VISIBLE
    }

    override fun onEmptyPhoneNumber() {
        mViewDataBinding.txtErrorPhone.text = "Xin hãy nhập thông tin"
        mViewDataBinding.txtErrorPhone.visibility = View.VISIBLE
    }

    override fun onWrongPhoneNumber() {
        mViewDataBinding.txtErrorPhone.text = "Xin hãy nhập đúng số điện thoại"
        mViewDataBinding.txtErrorPhone.visibility = View.VISIBLE
    }

    override fun onEmptyCustomerName() {
        mViewDataBinding.txtErrorUsername.text = "Xin hãy nhập thông tin"
        mViewDataBinding.txtErrorUsername.visibility = View.VISIBLE
    }
}