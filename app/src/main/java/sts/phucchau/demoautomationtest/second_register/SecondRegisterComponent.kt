package sts.phucchau.demoautomationtest.second_register

import sts.phucchau.data.common.AppComponent
import com.base.injection.scope.OutOfApplicationScope
import dagger.Component
import sts.phucchau.demoautomationtest.first_register.SecondRegisterFragment

@Component(modules = [(SecondRegisterModule::class)], dependencies = [(AppComponent::class)])
@OutOfApplicationScope
interface SecondRegisterComponent {
    fun inject(i: SecondRegisterFragment)
}