package sts.phucchau.demoautomationtest.first_register

import com.base.viewmodel.CommonView

interface SecondRegisterView: CommonView{

    fun onRegisterSuccessfully(code: Int)

    fun onRegisterFailed()

    fun onEmptyUserName()

    fun onEmptyPassword()

    fun onEmptyConfirmPassword()

    fun onWrongConfirmPassword()
}