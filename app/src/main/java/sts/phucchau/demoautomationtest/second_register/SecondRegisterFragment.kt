package sts.phucchau.demoautomationtest.first_register

import android.view.View
import android.widget.Toast
import com.base.fragment.BaseInjectingFragment
import sts.phucchau.data.domain.AppDomain
import sts.phucchau.data.model.Customer
import sts.phucchau.demoautomationtest.App
import sts.phucchau.demoautomationtest.R
import sts.phucchau.demoautomationtest.databinding.FragmentSecondRegisterBinding
import sts.phucchau.demoautomationtest.second_register.DaggerSecondRegisterComponent
import sts.phucchau.demoautomationtest.second_register.SecondRegisterComponent
import sts.phucchau.demoautomationtest.second_register.SecondRegisterViewModel
import javax.inject.Inject

class SecondRegisterFragment : BaseInjectingFragment<FragmentSecondRegisterBinding, SecondRegisterViewModel, SecondRegisterComponent>(), SecondRegisterView {
    override fun onWrongConfirmPassword() {
        mViewDataBinding.txtErrorPwd2.text = "Xin hãy nhập đúng mật khẩu"
        mViewDataBinding.txtErrorPwd2.visibility = View.VISIBLE
    }

    override fun onEmptyUserName() {
        mViewDataBinding.txtErrorUsername2.text = "Xin hãy nhập thông tin"
        mViewDataBinding.txtErrorUsername2.visibility = View.VISIBLE
    }

    override fun onEmptyPassword() {
        mViewDataBinding.txtErrorPwd.text = "Xin hãy nhập thông tin"
        mViewDataBinding.txtErrorPwd.visibility = View.VISIBLE
    }

    override fun onEmptyConfirmPassword() {
        mViewDataBinding.txtErrorPwd2.text = "Xin hãy nhập thông tin"
        mViewDataBinding.txtErrorPwd2.visibility = View.VISIBLE
    }

    override fun createComponent(): SecondRegisterComponent? {
        return DaggerSecondRegisterComponent.builder().appComponent(App.get(activity!!).component()).build()
    }

    override fun onInject(component: SecondRegisterComponent) {
        component.inject(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_second_register
    }

    override fun getViewModelClass(): Class<SecondRegisterViewModel>? {
        return SecondRegisterViewModel::class.java
    }

    override fun onVisible() {
        super.onVisible()
        mViewDataBinding.viewModel = mViewModel
    }

    @Inject
    fun setViewModelAttributes(appDomain: AppDomain) {
        mViewModel?.setViewModelAttributes(appDomain)
    }

    override fun onRegisterSuccessfully(code: Int) {
        when (code) {
            200 -> {
                fragmentManager!!.popBackStack()
                Toast.makeText(context, "Đăng ký thành công!", Toast.LENGTH_SHORT).show()
            }
            400 -> {
                Toast.makeText(context, "Đăng ký thất bại!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun setNewCustomer(customer: Customer) {
        mViewModel?.setNewCustomer(customer)
    }

    override fun onRegisterFailed() {
        Toast.makeText(context, "Đăng ký thất bại!", Toast.LENGTH_SHORT).show()
    }
}