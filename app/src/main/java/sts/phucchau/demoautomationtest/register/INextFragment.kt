package sts.phucchau.demoautomationtest.register

import android.widget.ImageView
import sts.phucchau.data.model.Customer

interface INextFragment {
    fun nextFragment(imgView: ImageView, customer: Customer)
}