package sts.phucchau.demoautomationtest.register

import com.base.viewmodel.CommonView
import sts.phucchau.data.model.Customer

interface RegisterView: CommonView {

    fun switchToFirst()

    fun switchToSecond(customer: Customer)
}