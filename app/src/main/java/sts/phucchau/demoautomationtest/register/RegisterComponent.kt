package sts.phucchau.demoautomationtest.register

import com.base.injection.scope.OutOfApplicationScope
import dagger.Component
import sts.phucchau.data.common.AppComponent

@Component(modules = [(RegisterModule::class)], dependencies = [(AppComponent::class)])
@OutOfApplicationScope
interface RegisterComponent {
    fun inject(i: RegisterFragment)
}