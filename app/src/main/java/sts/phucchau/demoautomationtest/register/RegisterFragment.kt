package sts.phucchau.demoautomationtest.register

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.widget.ImageView
import com.base.fragment.BaseInjectingFragment
import sts.phucchau.data.model.Customer
import sts.phucchau.demoautomationtest.App
import sts.phucchau.demoautomationtest.R
import sts.phucchau.demoautomationtest.databinding.FragmentRegisterBinding
import sts.phucchau.demoautomationtest.first_register.FirstRegisterFragment
import sts.phucchau.demoautomationtest.first_register.SecondRegisterFragment
import javax.inject.Inject

enum class STATE {
    FIRST_REGISTER,
    SECOND_REGISTER,
    ANIMATING
}

class RegisterFragment : BaseInjectingFragment<FragmentRegisterBinding, RegisterViewModel, RegisterComponent>(), RegisterView, INextFragment {
    private var state = STATE.FIRST_REGISTER

    @Inject
    lateinit var firstRegisterFragment: FirstRegisterFragment

    @Inject
    lateinit var secondRegisterFragment: SecondRegisterFragment


    override fun getLayoutId(): Int {
        return R.layout.fragment_register
    }

    override fun nextFragment(imgView: ImageView, customer: Customer) {
        if (state == STATE.FIRST_REGISTER) {
            switchToUser2(customer)
            imgView.setImageResource(R.drawable.icons8_chevronup)
        } else if (state == STATE.SECOND_REGISTER) {
            switchToUser1()
            imgView.setImageResource(R.drawable.icons8_chevrondown)
        }
    }

    override fun createComponent(): RegisterComponent? {
        return DaggerRegisterComponent.builder().appComponent(App.get(activity!!).component()).registerModule(RegisterModule(this)).build()
    }

    override fun onInject(component: RegisterComponent) {
        component.inject(this)
    }

    override fun getViewModelClass(): Class<RegisterViewModel>? {
        return RegisterViewModel::class.java
    }

    override fun onVisible() {
        super.onVisible()
        mViewDataBinding.viewModel = mViewModel
        initFragment()
    }

    private fun initFragment() {
        val fragmentManager = fragmentManager
        fragmentManager!!.beginTransaction()
                .replace(R.id.register1, firstRegisterFragment)
                .commit()

        fragmentManager.beginTransaction()
                .replace(R.id.register2, secondRegisterFragment)
                .commit()
    }

    override fun switchToFirst() {
        switchToUser1()
    }

    override fun switchToSecond(customer: Customer) {
        switchToUser2(customer)
    }

    private fun switchToUser2(customer: Customer) {
        if (state == STATE.FIRST_REGISTER) {
            val animatorFrame1 = ObjectAnimator.ofFloat(mViewDataBinding.register1, "translationY", -(App.maxHeight - App.maxHeight * 0.475f)).setDuration(400) //-1300
            val animatorFrame2 = ObjectAnimator.ofFloat(mViewDataBinding.register2, "translationY", -(App.maxHeight - App.maxHeight * 0.475f)).setDuration(400)

            val set = AnimatorSet()
            set.playTogether(animatorFrame1, animatorFrame2)
            set.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    state = STATE.ANIMATING
                }

                override fun onAnimationEnd(animation: Animator) {
                    state = STATE.SECOND_REGISTER
                    secondRegisterFragment.setNewCustomer(customer)
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })
            set.start()
        }
    }

    private fun switchToUser1() {
        if (state == STATE.SECOND_REGISTER) {
            val animatorFrame1 = ObjectAnimator.ofFloat(mViewDataBinding.register1, "translationY", 0f).setDuration(400)
            val animatorFrame2 = ObjectAnimator.ofFloat(mViewDataBinding.register2, "translationY", 0f).setDuration(400)

            val set = AnimatorSet()
            set.playTogether(animatorFrame1, animatorFrame2)
            set.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    state = STATE.ANIMATING
                }

                override fun onAnimationEnd(animation: Animator) {
                    state = STATE.FIRST_REGISTER
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })
            set.start()
        }
    }
}