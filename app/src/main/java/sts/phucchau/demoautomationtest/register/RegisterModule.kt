package sts.phucchau.demoautomationtest.register

import dagger.Module
import dagger.Provides
import sts.phucchau.demoautomationtest.first_register.FirstRegisterFragment
import sts.phucchau.demoautomationtest.first_register.SecondRegisterFragment

@Module
class RegisterModule (private val registerFragment:RegisterFragment) {

    @Provides
    fun getFirstFragment() : FirstRegisterFragment {
        return FirstRegisterFragment(registerFragment)
    }

    @Provides
    fun getSecondFragment() : SecondRegisterFragment {
        return SecondRegisterFragment()
    }

    @Provides
    fun nextFragment() : INextFragment {
        return registerFragment
    }
}