package sts.phucchau.demoautomationtest.main

import android.os.Bundle
import android.view.WindowManager
import com.base.activity.BaseActivity
import sts.phucchau.demoautomationtest.R
import sts.phucchau.demoautomationtest.databinding.ActivityMainBinding
import sts.phucchau.demoautomationtest.home.HomeFragment

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), MainView {
    override fun getViewModelClass(): Class<MainViewModel>? {
        return MainViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        initFragment()
    }

    private fun initFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.main_frame, HomeFragment())
                .commit()
    }
}