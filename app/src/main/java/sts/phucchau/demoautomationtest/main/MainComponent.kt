package sts.phucchau.demoautomationtest.main

import sts.phucchau.data.common.AppComponent
import com.base.injection.scope.OutOfApplicationScope
import dagger.Component

@Component(modules = [(MainModule::class)], dependencies = [(AppComponent::class)])
@OutOfApplicationScope
interface MainComponent {
    fun inject(i: MainActivity)
}