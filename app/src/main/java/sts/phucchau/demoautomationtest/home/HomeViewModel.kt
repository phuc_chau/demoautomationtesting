package sts.phucchau.demoautomationtest.home

import com.base.viewmodel.FragmentViewModel

class HomeViewModel : FragmentViewModel() {
    fun onShowLogin() {
        val view: HomeView? = view()
        view?.onShowLogin()
    }

    fun onShowDrawer() {
        val view: HomeView? = view()
        view?.onShowDrawer()
    }

    fun onHideDrawer() {
        val view: HomeView? = view()
        view?.onHideDrawer()
    }

    fun openMenu() {
        val view: HomeView? = view()
        view?.openMenu()
    }

    fun closeMenu() {
        val view: HomeView? = view()
        view?.closeMenu()
    }
}