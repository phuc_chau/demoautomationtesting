package sts.phucchau.demoautomationtest.home

import sts.phucchau.data.common.AppComponent
import com.base.injection.scope.OutOfApplicationScope
import dagger.Component

@Component(modules = [(HomeModule::class)], dependencies = [(AppComponent::class)])
@OutOfApplicationScope
interface HomeComponent{
    fun inject(f: HomeFragment)
}