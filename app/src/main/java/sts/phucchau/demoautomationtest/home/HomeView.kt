package sts.phucchau.demoautomationtest.home

import com.base.viewmodel.CommonView

interface HomeView: CommonView{

    fun onShowLogin()

    fun onShowDrawer()

    fun onHideDrawer()

    fun openMenu()

    fun closeMenu()
}