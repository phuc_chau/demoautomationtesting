package sts.phucchau.demoautomationtest.home

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.os.Handler
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.base.fragment.BaseInjectingFragment
import sts.phucchau.demoautomationtest.App
import sts.phucchau.demoautomationtest.R
import sts.phucchau.demoautomationtest.databinding.FragmentHomeBinding
import sts.phucchau.demoautomationtest.login.LoginFragment
import sts.phucchau.demoautomationtest.utils.UI
import sts.phucchau.demoautomationtest.utils.Views

class HomeFragment : BaseInjectingFragment<FragmentHomeBinding, HomeViewModel, HomeComponent>(), HomeView {

    val DRAWER_OPENED = 4
    val DRAWER_OPENING = 3
    val DRAWER_CLOSING = 2
    val DRAWER_CLOSED = 1
    internal var currentDrawerState = DRAWER_CLOSED
    private var transferMenuState: MenuState? = null

    override fun createComponent(): HomeComponent? {
        return DaggerHomeComponent.builder().appComponent(App.get(activity!!).component()).build()
    }

    override fun onInject(component: HomeComponent) {
        component.inject(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun getViewModelClass(): Class<HomeViewModel>? {
        return HomeViewModel::class.java
    }

    override fun onShowDrawer() {
        if (currentDrawerState == DRAWER_CLOSED) {
            currentDrawerState = DRAWER_OPENING

            val moveToX = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "translationX", App.maxWidth * 3f / 5)
            val scaleX = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "scaleX", .9f)
            val scaleY = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "scaleY", .9f)
            val rotateY = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "rotationY", -20f)

            val set = AnimatorSet()
            set.interpolator = AccelerateDecelerateInterpolator()
            set.play(moveToX).with(scaleX).with(scaleY).with(rotateY)
            set.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    mViewDataBinding.mainFrameMask.setImageDrawable(BitmapDrawable(resources, Bitmap.createBitmap(screenShot(mViewDataBinding.fullContent))))
                    mViewDataBinding.mainFrameMask.visibility = View.VISIBLE
                    mViewDataBinding.homeFrame.visibility = View.GONE
                }

                override fun onAnimationEnd(animation: Animator) {
                    currentDrawerState = DRAWER_OPENED
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })
            set.start()
        }
    }

    override fun onHideDrawer() {
        if (currentDrawerState == DRAWER_OPENED) {
            currentDrawerState = DRAWER_CLOSING

            val moveToX = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "translationX", 0f)
            val scaleX = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "scaleX", 1f)
            val scaleY = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "scaleY", 1f)
            val rotateY = ObjectAnimator.ofFloat(mViewDataBinding.mainLayout, "rotationY", 0f)

            val set = AnimatorSet()
            set.interpolator = AccelerateDecelerateInterpolator()
            set.play(moveToX).with(scaleX).with(scaleY).with(rotateY)
            set.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {

                }

                override fun onAnimationEnd(animation: Animator) {
                    mViewDataBinding.mainFrameMask.visibility = View.GONE
                    mViewDataBinding.homeFrame.visibility = View.VISIBLE
                    currentDrawerState = DRAWER_CLOSED
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })
            set.start()
        }
    }

    override fun onShowLogin() {
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.add(R.id.main_frame, LoginFragment()).addToBackStack("home")
                .commit()
    }

    override fun openMenu() {
        showTransferMenu()
    }

    override fun closeMenu() {
        hideTransferMenu()
    }

    fun screenShot(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.width,
                view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    override fun onStart() {
        super.onStart()
        currentDrawerState = 1
    }

    override fun onVisible() {
        super.onVisible()
        mViewDataBinding.viewModel = mViewModel
        initProfileDrawer()
        setupBlurBG()
    }

    private fun initProfileDrawer() {
        /*if (maKhachHang != 0) {
            logoutLayout.setVisibility(View.VISIBLE)
            userNameTV.setText(activity!!.getSharedPreferences("WEDDING", Context.MODE_PRIVATE).getString("tenKhachHang", "null"))
        }*/
    }

    private fun setupBlurBG() {
        setTransferMenuState(MenuState.GONE)
        mViewDataBinding.bgTransferBlur.visibility = View.GONE
    }

    private fun setBlurBG() {
        Handler().post {
            run {
                mViewDataBinding.bgTransferBlur.background = BitmapDrawable(resources, blur(0.4f, 25f))
            }
        }
    }

    private fun setTransferMenuState(state: MenuState) {
        transferMenuState = state
    }

    private fun showTransferMenu() {
        if (transferMenuState == MenuState.GONE) {
            val rotateBtnPay = ObjectAnimator.ofFloat(mViewDataBinding.rlBtnMenu, "rotation", 0f, 360f).setDuration(400)
            val scaleXBtnPay = ObjectAnimator.ofFloat(mViewDataBinding.rlBtnMenu, "scaleX", 1f, 0f).setDuration(400)
            val scaleYBtnPay = ObjectAnimator.ofFloat(mViewDataBinding.rlBtnMenu, "scaleY", 1f, 0f).setDuration(400)

            val alphaBG = ObjectAnimator.ofFloat(mViewDataBinding.bgTransferBlur, "alpha", 0f, 1f).setDuration(200)
            alphaBG.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    mViewDataBinding.bgTransferBlur.visibility = View.VISIBLE
                }

                override fun onAnimationEnd(animation: Animator) {

                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })

            val moveMoneyTranslation = ObjectAnimator.ofFloat(mViewDataBinding.layout1, "translationY", UI.dp(100).toFloat(), 0f).setDuration(75)
            val moveMoneyAlpha = ObjectAnimator.ofFloat(mViewDataBinding.layout1, "alpha", 0f, 1f).setDuration(75)
            val setMoveMoney = AnimatorSet()
            setMoveMoney.playTogether(moveMoneyTranslation, moveMoneyAlpha)
            setMoveMoney.startDelay = 150

            val topUpTranslation = ObjectAnimator.ofFloat(mViewDataBinding.layout2, "translationY", UI.dp(100).toFloat(), 0f).setDuration(75)
            val topUpAlpha = ObjectAnimator.ofFloat(mViewDataBinding.layout2, "alpha", 0f, 1f).setDuration(75)
            val setTopUp = AnimatorSet()
            setTopUp.playTogether(topUpTranslation, topUpAlpha)
            setTopUp.startDelay = 100

            val closeTranslation = ObjectAnimator.ofFloat(mViewDataBinding.close, "translationY", UI.dp(100).toFloat(), 0f).setDuration(75)
            val closeAlpha = ObjectAnimator.ofFloat(mViewDataBinding.close, "alpha", 0f, 1f).setDuration(75)
            val setClose = AnimatorSet()
            setClose.playTogether(closeTranslation, closeAlpha)

            val setMenuFull = AnimatorSet()
            setMenuFull.playTogether(setClose, setTopUp, setMoveMoney, alphaBG)
            setMenuFull.startDelay = 75

            setMenuFull.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    mViewDataBinding.layout1.alpha = 0f
                    mViewDataBinding.layout2.alpha = 0f
                    mViewDataBinding.close.alpha = 0f
                }

                override fun onAnimationEnd(animation: Animator) {}

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })

            val set = AnimatorSet()
            set.playTogether(rotateBtnPay, scaleXBtnPay, scaleYBtnPay, setMenuFull)
            set.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    setTransferMenuState(MenuState.INVALIDATE)
                    mViewDataBinding.bgTransferBlur.visibility = View.INVISIBLE
                    setBlurBG()
                }

                override fun onAnimationEnd(animation: Animator) {
                    setTransferMenuState(MenuState.VISIBLE)
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })
            set.start()
        }
    }

    private fun hideTransferMenu() {
        if (transferMenuState == MenuState.VISIBLE) {
            val rotateBtnPay = ObjectAnimator.ofFloat(mViewDataBinding.rlBtnMenu, "rotation", 360f, 0f).setDuration(400)
            val scaleXBtnPay = ObjectAnimator.ofFloat(mViewDataBinding.rlBtnMenu, "scaleX", 0f, 1f).setDuration(400)
            val scaleYBtnPay = ObjectAnimator.ofFloat(mViewDataBinding.rlBtnMenu, "scaleY", 0f, 1f).setDuration(400)
            val alphaBG = ObjectAnimator.ofFloat(mViewDataBinding.bgTransferBlur, "alpha", 1f, 0f).setDuration(200)
            alphaBG.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {

                }

                override fun onAnimationEnd(animation: Animator) {
                    mViewDataBinding.bgTransferBlur.visibility = View.GONE
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })
            val setBtnPay = AnimatorSet()
            setBtnPay.playTogether(rotateBtnPay, scaleXBtnPay, scaleYBtnPay, alphaBG)
            val set = AnimatorSet()
            set.playTogether(alphaBG, setBtnPay)
            set.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    setTransferMenuState(MenuState.INVALIDATE)
                    Handler().postDelayed({
                        if (transferMenuState == MenuState.INVALIDATE)
                            setTransferMenuState(MenuState.GONE)
                    }, 500)
                }

                override fun onAnimationEnd(animation: Animator) {
                    setTransferMenuState(MenuState.GONE)
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }
            })
            set.start()
        }
    }

    //---< Transfer Menu >---//
    private enum class MenuState {
        GONE, INVALIDATE, VISIBLE
    }

    private fun blur(scale: Float, radius: Float): Bitmap {
        return Views.blur(mViewDataBinding.root, scale, radius)
    }
}