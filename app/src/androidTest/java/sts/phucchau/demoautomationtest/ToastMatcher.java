package sts.phucchau.demoautomationtest;

import android.os.IBinder;
import android.support.test.espresso.Root;
import android.view.WindowManager;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by BelP on 14/11/2017.
 */

public class ToastMatcher extends TypeSafeMatcher<Root> {
    private String resultExpected;

    public ToastMatcher(String resultExpected){
        this.resultExpected = resultExpected;
    }

    @Override
    public boolean matchesSafely(Root root) {
        int type = root.getWindowLayoutParams().get().type;
        if ((type == WindowManager.LayoutParams.TYPE_TOAST)) {
            IBinder windowToken = root.getDecorView().getWindowToken();
            IBinder appToken = root.getDecorView().getApplicationWindowToken();
            return windowToken == appToken;
        }
        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(resultExpected);
    }
}
