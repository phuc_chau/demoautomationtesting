package sts.phucchau.demoautomationtest

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import org.hamcrest.Matcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import sts.phucchau.demoautomationtest.main.MainActivity
import sts.phucchau.demoautomationtest.second_register.SecondRegisterViewModel

/**
 * Created by BelP on 27/07/2018.
 */

@RunWith(AndroidJUnit4::class)
class RegisterUITest {

    @get:Rule
    var mActivityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    private val secondRegisterViewModel =  SecondRegisterViewModel()

    @Before
    @Throws(Exception::class)
    fun setUp() {
        onView(withId(R.id.btn_drawer)).perform(click())
        waitFor(3000)
        onView(withId(R.id.loginLayout)).perform(click())
        waitFor(3000)
        onView(withId(R.id.btn_forgot)).perform(click())
    }

    @Test
    fun testCase1() {
        register("", "01207594594", "phucne@gmail.com", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase2() {
        register("phuc", "", "phucne@gmail.com", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase3() {
        register("phuc", "123", "phucne@gmail.com", "", "", "",
                "Xin hãy nhập đúng số điện thoại")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase4() {
        register("phuc", "01207594594", "", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase5() {
        register("phuc", "01207594594", "phucne@gmail.com", "", "123456", "123456",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase6() {
        register("phuc", "01207594594", "phucne@gmail.com", "phucne", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase7() {
        register("phuc", "01207594594", "phucne@gmail.com", "phucne", "123456", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase8() {
        register("phuc", "01207594594", "phucne@gmail.com", "phuccc", "12345", "123456",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase9() {
        register("phuc", "01207594594", "phucne@gmail.com", "phucminh4", "123456", "123456",
                "Đăng ký thành công!")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase17() {
        register("phuc", "01207594594", "phucne", "phucminh4", "123456", "123456",
                "Xin hãy nhập đúng email")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase10() {
        register("", "", "hcm", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    @Test
    fun testCase11() {
        register("phuc", "", "", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    fun testCase12() {
        register("phuc", "", "phucne@gmail.com", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    fun testCase13() {
        register("phuc", "01207594594", "phucne@gmail.com", "", "", "123456",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    fun testCase14() {
        register("", "123", "phucne@gmail.com", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    fun testCase15() {
        register("phuc", "123", "", "", "", "",
                "Xin hãy nhập thông tin")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }

    fun testCase16() {
        register("phuc", "123", "phucne@gmail.com", "", "", "",
                "Xin hãy nhập đúng số điện thoại")
        onView(isRoot()).perform(waitFor(2000))
        resetFragment()
    }


    private fun resetFragment() {
        onView(withId(R.id.backBtn)).perform(click())

        onView(isRoot()).perform(waitFor(1000))
    }

    @Throws(Exception::class)
    private fun register(name: String, phone: String, email: String,
                         userName: String, password: String, rePassword: String, resultExpected: String) {
        onView(withId(R.id.userNameET)).perform(clearText(),
                typeText(name))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.mobilePhoneET)).perform(clearText(),
                typeText(phone))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.emailET)).perform(clearText(),
                typeText(email))
        Espresso.closeSoftKeyboard()

        onView(withId(R.id.backBtn)).perform(click())
        if (name == "" || phone == "" || email == "") {
            when {
                name == "" -> {
                    onView(withId(R.id.txt_error_username)).check(matches(isDisplayed()))
                    onView(withId(R.id.txt_error_username)).check(matches(withText(resultExpected)))
                    return
                }
                phone == "" -> {
                    onView(withId(R.id.txt_error_phone)).check(matches(isDisplayed()))
                    onView(withId(R.id.txt_error_phone)).check(matches(withText(resultExpected)))
                    return
                }
                email == "" -> {
                    onView(withId(R.id.txt_error_email)).check(matches(isDisplayed()))
                    onView(withId(R.id.txt_error_email)).check(matches(withText(resultExpected)))
                    return
                }
                else -> {
                }
            }
        } else {
            if (phone.length in 1..10) {
                onView(withId(R.id.txt_error_phone)).check(matches(isDisplayed()))
                onView(withId(R.id.txt_error_phone)).check(matches(withText(resultExpected)))
                return
            }
            if (!secondRegisterViewModel.onValidateEmail(email)) {
                onView(withId(R.id.txt_error_email)).check(matches(isDisplayed()))
                onView(withId(R.id.txt_error_email)).check(matches(withText(resultExpected)))
                return
            }
        }

        onView(withId(R.id.userAccountET)).perform(clearText(),
                typeText(userName))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.passwordET)).perform(clearText(),
                typeText(password))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.rePasswordET)).perform(clearText(),
                typeText(rePassword))
        Espresso.closeSoftKeyboard()

        onView(withId(R.id.registerBtn)).perform(click())

        if (userName == "" || password == "" || rePassword == "") {
            when {
                userName == "" -> {
                    onView(withId(R.id.txt_error_username2)).check(matches(isDisplayed()))
                    onView(withId(R.id.txt_error_username2)).check(matches(withText(resultExpected)))
                    return
                }
                password == "" -> {
                    onView(withId(R.id.txt_error_pwd)).check(matches(isDisplayed()))
                    onView(withId(R.id.txt_error_pwd)).check(matches(withText(resultExpected)))
                    return
                }
                rePassword == "" -> {
                    onView(withId(R.id.txt_error_pwd2)).check(matches(isDisplayed()))
                    onView(withId(R.id.txt_error_pwd2)).check(matches(withText(resultExpected)))
                    return
                }
                else -> {
                }
            }
        } else {
            if (rePassword != password) {
                return
            }
        }

        onView(withText(resultExpected)).inRoot(ToastMatcher(resultExpected))
                .check(matches(withText(resultExpected)))
    }

    private fun waitFor(millis: Long): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isRoot()
            }

            override fun getDescription(): String {
                return "Wait for $millis milliseconds."
            }

            override fun perform(uiController: UiController, view: View) {
                uiController.loopMainThreadForAtLeast(millis)
            }
        }
    }
}