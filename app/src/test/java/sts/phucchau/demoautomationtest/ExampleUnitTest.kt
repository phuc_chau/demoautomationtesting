package sts.phucchau.demoautomationtest

import android.content.Context
import junit.framework.Assert.assertEquals
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mockito.mock
import sts.phucchau.demoautomationtest.second_register.SecondRegisterViewModel

class ExampleUnitTest {

    lateinit var viewModel: SecondRegisterViewModel
    lateinit var mContext: Context

    @BeforeClass
    fun setUpClass() {
        mContext = mock(Context::class.java)
        viewModel = SecondRegisterViewModel()
    }

    @Test
    fun addition_isCorrect() {

        assertEquals("true", true, viewModel.onValidateEmail("pucgaoxam@gmail.com"))

    }
}
