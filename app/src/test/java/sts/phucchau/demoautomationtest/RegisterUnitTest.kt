package sts.phucchau.demoautomationtest

import com.google.gson.Gson
import io.reactivex.schedulers.TestScheduler
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import sts.phucchau.data.common.NetworkModule
import sts.phucchau.data.common.ServerServices
import sts.phucchau.data.model.Customer


@RunWith(JUnit4::class)
class RegisterUnitTest {
    private var retrofit: Retrofit? = null
    private lateinit var mServerServices: ServerServices
    private val testAndroidSchedulers = TestScheduler()

    @Before
    fun setUp() {
        val builder = Retrofit.Builder()
                .baseUrl(NetworkModule.DEV_DOMAIN)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        retrofit = builder.build()
        mServerServices = retrofit!!.create(ServerServices::class.java)
    }

    @Test
    fun testRegister1() {
        //Case 1
        val customer = Customer(0, "", "01207594594", "phucne@gmail.com", "", "123")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister2() {
        val customer = Customer(0, "phuc", "", "phucne@gmail.com", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister3() {
        val customer = Customer(0, "phuc", "", "phucne@gmail.com", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister4() {
        val customer = Customer(0, "phuc", "01207594594", "", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister5() {
        val customer = Customer(0, "phuc", "01207594594", "phucne@gmail.com", "", "123456")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister6() {
        val customer = Customer(0, "phuc", "01207594594", "phucne@gmail.com", "phucne", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister7() {
        val customer = Customer(0, "phuc", "01207594594", "phucne@gmail.com", "phucne", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    //Case 8 -- Nhập không trùng dữ liệu <=> không có password
    @Test
    fun testRegister8() {
        val customer = Customer(0, "phuc", "01207594594", "phucne@gmail.com", "phucne", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister9() {
        val customer = Customer(0, "phuc", "01207594594", "phucne@gmail.com", "phucnehihi", "123456")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(200, it) }, {})
    }

    @Test
    fun testRegister10() {
        val customer = Customer(0, "", "", "phucne@gmail.com", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister11() {
        val customer = Customer(0, "phuc", "", "phucne@gmail.com", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister12() {
        val customer = Customer(0, "phuc", "", "", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister13() {
        val customer = Customer(0, "phuc", "01207594594", "phucne@gmail.com", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister14() {
        val customer = Customer(0, "", "123", "phucne@gmail.com", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister15() {
        val customer = Customer(0, "phuc", "123", "", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }

    @Test
    fun testRegister16() {
        val customer = Customer(0, "phuc", "123", "phucne@gmail.com", "", "")
        mServerServices.register(customer).observeOn(testAndroidSchedulers)
                .subscribe({ Assert.assertEquals(400, it) }, {})
    }
}