package sts.phucchau.demoautomationtest

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import sts.phucchau.demoautomationtest.second_register.SecondRegisterViewModel

/**
 * Created by BelP on 27/07/2018.
 */
@RunWith(JUnit4::class)
class EmailValidateUnitTest {

    private val viewModel: SecondRegisterViewModel = SecondRegisterViewModel()

    @Test
    fun case1() {
        //assertEquals(true, viewModel.onValidateEmail("phuc.chau@gmail.com"))
    }

    @Test
    fun case2() {
        //assertEquals(false, viewModel.onValidateEmail("phuc"))
    }

    @Test
    fun case3() {
        //assertEquals(false, viewModel.onValidateEmail("phuc.@gmail.com"))
    }

    @Test
    fun case4() {
        //assertEquals(false, viewModel.onValidateEmail("phuc@.gmail.com"))
    }

}