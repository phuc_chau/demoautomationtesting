package sts.phucchau.data.common

import com.base.imageloader.ImageLoader
import com.base.injection.module.ImageLoaderModule
import com.base.injection.scope.ApplicationScope
import dagger.Component
import sts.phucchau.data.repository.hall.HallRepository
import sts.phucchau.data.repository.user.UserRepository

/**
 * Created by vophamtuananh on 3/13/18.
 */

@Component(modules = [ImageLoaderModule::class, RepositoryModule::class, AppModule::class])
@ApplicationScope
interface AppComponent {

    fun imageLoader(): ImageLoader

    fun sharePreferenceManager(): SharePreferenceManager

    fun userRepository(): UserRepository

    fun hallRepository(): HallRepository
}