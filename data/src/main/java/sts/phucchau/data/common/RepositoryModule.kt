package sts.phucchau.data.common

import com.base.injection.scope.ApplicationScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import sts.phucchau.data.repository.hall.HallRepository
import sts.phucchau.data.repository.hall.HallRepositoryImpl
import sts.phucchau.data.repository.user.UserRepository
import sts.phucchau.data.repository.user.UserRepositoryImpl

/**
 * Created by vophamtuananh on 3/13/18.
 */

@Module(includes = [NetworkModule::class])
class RepositoryModule {

    @Provides
    @ApplicationScope
    fun userRepository(serverServices: ServerServices): UserRepository {
        return UserRepositoryImpl(serverServices)
    }

    @Provides
    @ApplicationScope
    fun hallRepository(serverServices: ServerServices): HallRepository {
        return HallRepositoryImpl(serverServices)
    }

    @Provides
    @ApplicationScope
    fun serverServices(retrofit: Retrofit): ServerServices {
        return retrofit.create(ServerServices::class.java)
    }

}