package sts.phucchau.data.common

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import sts.phucchau.data.model.Customer
import sts.phucchau.data.model.Hall

/**
 * Created by vophamtuananh on 3/13/18.
 */
interface ServerServices {

    companion object {
        const val IMAGE_TYPE = "image/*"
    }

    @GET("/api/KhachHangs/Login/")
    fun login(@Query("username") userName: String, @Query("password") password: String): Single<Int>

    @POST("/api/KhachHangs/Register")
    fun register(@Body customer: Customer): Single<Int>

    @GET("/api/Sanhs/GetSanh")
    fun getHalls(): Single<List<Hall>>
}