package sts.phucchau.data.repository.hall

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import sts.phucchau.data.common.ServerServices
import sts.phucchau.data.model.Hall

class HallRepositoryImpl(private val mServerServices: ServerServices) : HallRepository {
    override fun getHalls(): Single<List<Hall>> {
        return mServerServices.getHalls()
                .filter { it != null}
                .toSingle()
                .map { it }
                .subscribeOn(Schedulers.io())
    }
}