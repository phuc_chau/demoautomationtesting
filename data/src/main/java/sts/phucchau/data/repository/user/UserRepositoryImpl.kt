package sts.phucchau.data.repository.user

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import sts.phucchau.data.common.ServerServices
import sts.phucchau.data.model.Customer
import sts.phucchau.data.model.response.ResponseLogin

class UserRepositoryImpl(private val mServerServices: ServerServices) : UserRepository {
    override fun login(userName: String, password: String): Single<Int> {
        return mServerServices.login(userName, password)
                .filter { true }
                .toSingle()
                .map { it!! }
                .observeOn(Schedulers.io())
    }

    override fun register(customer: Customer): Single<Int> {
        return mServerServices.register(customer)
                .filter { true }
                .toSingle()
                .map { it!! }
                .observeOn(Schedulers.io())
    }
}