package sts.phucchau.data.repository.hall

import io.reactivex.Single
import sts.phucchau.data.model.Hall

interface HallRepository {


    fun getHalls() : Single<List<Hall>>
}