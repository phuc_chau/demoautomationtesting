package sts.phucchau.data.repository.user

import io.reactivex.Single
import sts.phucchau.data.model.Customer

interface UserRepository {
    fun login(userName: String, password: String) : Single<Int>

    fun register(customer: Customer) : Single<Int>
}