package sts.phucchau.data.model

import com.google.gson.annotations.SerializedName

data class Customer(
        @SerializedName("MaKhachHang") var customerId: Int = 0,
        @SerializedName("TenKhachHang") var customerName: String = "",
        @SerializedName("DienThoai") var phoneNum: String = "",
        @SerializedName("DiaChi") var email: String = "",
        @SerializedName("TenTaiKhoan") var userName: String = "",
        @SerializedName("MatKhau") var password: String = ""
)