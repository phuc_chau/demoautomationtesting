package sts.phucchau.data.model

import com.google.gson.annotations.SerializedName

data class HallType(
        @SerializedName("MaLoaiSanh") val hallTypeId: Int,
        @SerializedName("TenLoaiSanh") val hallTypeName: String,
        @SerializedName("DonGiaBanToiThieu") val hallTypeMinimumPrice: String
)