package sts.phucchau.data.model.response

import com.google.gson.annotations.SerializedName
import sts.phucchau.data.model.Customer

data class ResponseLogin(@SerializedName("resultCode") val resultCode: Int,
                         @SerializedName("khachHang") val customer: Customer)