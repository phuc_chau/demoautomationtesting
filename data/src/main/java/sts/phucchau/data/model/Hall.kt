package sts.phucchau.data.model

import com.google.gson.annotations.SerializedName

data class Hall(
        @SerializedName("MaSanh") val hallId: Int,
        @SerializedName("TenSanh") val hallName: String,
        @SerializedName("HinhAnh") val hallImage: String,
        @SerializedName("MaLoaiSanh") val hallTypeId: Int,
        @SerializedName("SoLuongBanToiDa") val maximumTable: Int,
        @SerializedName("GhiChu") val note: String
)