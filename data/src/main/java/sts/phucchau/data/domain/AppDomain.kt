package sts.phucchau.data.domain

import io.reactivex.Single
import sts.phucchau.data.model.Customer
import sts.phucchau.data.model.Hall
import sts.phucchau.data.repository.hall.HallRepository
import sts.phucchau.data.repository.user.UserRepository
import javax.inject.Inject

/**
 * Created by vophamtuananh on 3/13/18.
 */
class AppDomain @Inject constructor(private val userRepository: UserRepository,
                                    private val hallRepository: HallRepository) {

    fun login(userName: String, password: String): Single<Int> {
        return userRepository.login(userName, password)
    }

    fun register(customer: Customer): Single<Int> {
        return userRepository.register(customer)
    }

    fun getHalls(): Single<List<Hall>> {
        return hallRepository.getHalls()
    }

    fun clean() {
    }
}